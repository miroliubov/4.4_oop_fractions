package src.models;

public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction(int nominator, int denominator) {
        this.nominator = nominator;
        if (denominator == 0){
            System.out.println("Ошибка! Знаменатель не может быть равен 0");
        }
        else {
            this.denominator = denominator;
        }
    }

    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public String toString() {
        return nominator + "/" + denominator;
    }
}
