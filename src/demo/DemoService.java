package src.demo;

import src.models.Fraction;
import src.service.CalculationService;
import src.service.ICalculationService;

public class DemoService implements IDemoService {

    @Override
    public void execute() {

        ICalculationService calculationService = new CalculationService();

        Fraction fraction1 = new Fraction(9, 36);
        System.out.println("Упрощение дроби " + fraction1.getNominator() + "/" + fraction1.getDenominator() + " = "
                + calculationService.simplification(fraction1));

        Fraction fraction2 = new Fraction(332, 112);
        System.out.println("Упрощение дроби " + fraction2.getNominator() + "/" + fraction2.getDenominator() + " = "
                + calculationService.simplification(fraction2));

        System.out.println("Сложение дробей " + fraction1 + " и " + fraction2 + " = "
                + calculationService.addition(fraction1, fraction2));

        System.out.println("Вычитание дроби " + fraction1 + " и " + fraction2 + " = "
                + calculationService.subtraction(fraction1, fraction2));

        System.out.println("Умножение дробей " + fraction1 + " и " + fraction2 + " = "
                + calculationService.multiplication(fraction1, fraction2));

        System.out.println("Деление дробей " + fraction1 + " и " + fraction2 + " = "
                + calculationService.division(fraction1, fraction2));


    }

}
