package src.service;

import src.models.Fraction;

public class CalculationService implements ICalculationService {

    @Override
    public Fraction simplification(Fraction fraction) {
        int gcd = GCD(fraction.getNominator(), fraction.getDenominator());
        return new Fraction((fraction.getNominator() / gcd), (fraction.getDenominator()/ gcd));
    }

    @Override
    public int GCD(int a, int b) {
        return b == 0 ? a : GCD(b, a % b);
    }

    @Override
    public Fraction addition(Fraction fraction1, Fraction fraction2) {
        int newNominator = fraction1.getNominator() * fraction2.getDenominator() +
                fraction1.getDenominator() * fraction2.getNominator();
        int newDenominator = fraction1.getDenominator() * fraction2.getDenominator();

        return simplification(new Fraction(newNominator, newDenominator));
    }

    @Override
    public Fraction subtraction(Fraction fraction1, Fraction fraction2) {
        int newNominator = fraction1.getNominator() * fraction2.getDenominator() -
                fraction1.getDenominator() * fraction2.getNominator();
        int newDenominator = fraction1.getDenominator() * fraction2.getDenominator();

        return simplification(new Fraction(newNominator, newDenominator));
    }

    @Override
    public Fraction multiplication(Fraction fraction1, Fraction fraction2) {
        return simplification(new Fraction(fraction1.getNominator() * fraction2.getNominator(),
                fraction1.getDenominator() * fraction2.getDenominator()));
    }

    @Override
    public Fraction division(Fraction fraction1, Fraction fraction2) {
        return simplification(new Fraction(fraction1.getNominator() * fraction2.getDenominator(),
                fraction1.getDenominator() * fraction2.getNominator()));
    }

}
