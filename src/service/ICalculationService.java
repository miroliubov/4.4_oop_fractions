package src.service;

import src.models.Fraction;

public interface ICalculationService {

    Fraction simplification(Fraction fraction);
    Fraction addition(Fraction fraction1, Fraction fraction2);
    Fraction subtraction(Fraction fraction1, Fraction fraction2);
    Fraction multiplication(Fraction fraction1, Fraction fraction2);
    Fraction division(Fraction fraction1, Fraction fraction2);
    int GCD(int a, int b);

}
